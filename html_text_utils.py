import logging

from bs4 import BeautifulSoup

LOGGER = logging.getLogger(__name__)


def split_param(query_str: str, start_str: str, end_str: str) -> str:
    start = query_str.find(start_str) + len(start_str)
    start_sub = query_str[start:]
    end = start_sub.find(end_str)
    return start_sub[:end]


def to_bs(html_str: str) -> BeautifulSoup:
    html = BeautifulSoup(html_str, "html5lib")
    return html
