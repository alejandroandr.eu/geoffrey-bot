import arrow
import httpx
from utils import async_run


@async_run
async def get_barcelona_data() -> str:
    barcelona_id = "48887"
    result = await get_city_data(barcelona_id)
    return result


async def get_city_data(city_id: str) -> str:
    today = arrow.now().format("YYYYMMDD")
    result = await get_wind_guru_data(city_id, today)
    clean_result = wind_guru_result_clean(result)
    return f"result: {clean_result}"


def _get_wind_guru_headers() -> dict:
    return {
        #"sec-ch-ua": ' Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "sec-ch-ua-mobile": "?0",
        "Referer": "https://www.windguru.cz/",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"
    }


async def get_wind_guru_data(city_id: str, day: str) -> dict:
    response: httpx.Response
    client: httpx.AsyncClient
    try:
        async with httpx.AsyncClient() as client:
            model = "3"
            url = (
                f"https://www.windguru.net/int/iapi.php?"
                f"q=forecast&"
                f"id_model={model}&"
                f"rundef={day}06x0x240x0x240&"
                f"initstr={day}06&"
                f"id_spot={city_id}&"
                f"WGCACHEABLE=21600&"
                f"cachefix=41.379x2.193x0&"
                f"_mha=9e154da5"
            )
            headers = _get_wind_guru_headers()
            response = await client.get(
                url,
                headers=headers
            )
            return response.json()
    except httpx.ConnectTimeout:
        print("Timeout")
        return {}


def wind_guru_result_clean(wind_guru_result: dict) -> str:
    '''
    APCP: LLuvia mm/3h
    APCP1: Lluvia mm/1h
    FLHGT
    GUST: Ráfagas viento
    HCDC
    LCDC
    MCDC
    PCPT
    RH: Huedad %
    SLHGT
    SLP: Presión hPa
    TCDC
    TMP: Temperatura
    TMPE
    WINDDIR: Direccion viento
    WINDSPD: Fuera viento
    WCHILL: Sensación termica
    hours
    '''
    if "fcst" in wind_guru_result:
        fcst = wind_guru_result["fcst"]
        now = arrow.now()
        hour = now.datetime.hour
        h_result = fcst["hours"][hour]
        rain = fcst["APCP1"][hour]
        rain_1 = fcst["APCP1"][hour + 1]
        rain_2 = fcst["APCP1"][hour + 2]
        rain_3 = fcst["APCP1"][hour + 3]
        rain_4 = fcst["APCP1"][hour + 4]
        humidity = fcst["RH"][hour]
        tmp = fcst["TMP"][hour]
        return f"Fa {tmp} ºC, una humitat del {humidity} %, i pluja en les próximes hores {rain_1}, {rain_2}, {rain_3}, {rain_4}"
        #  import pdb; pdb.set_trace()
        # for idx, row in enumerate(fcst["hours"]):
    else:
        return ""


