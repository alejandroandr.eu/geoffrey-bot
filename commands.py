import logging

from telegram import ForceReply, Update
from telegram.ext import CallbackContext

from btc_data import generate_btc_wallet, get_top_wallets
from wind_guru import get_barcelona_data

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, _: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    assert user
    assert update.message
    update.message.reply_markdown_v2(
        fr"Hi {user.mention_markdown_v2()}\!",
        reply_markup=ForceReply(selective=True),
    )


def prices(update: Update, _: CallbackContext) -> None:
    """Get top bitcoin wallet prices"""
    # user = update.effective_user
    assert update.message
    update.message.reply_text(get_top_wallets())


def play(update: Update, _: CallbackContext) -> None:
    """Generate Bitcoin address getting his balance"""
    user = update.effective_user
    assert update.message
    assert user
    update.message.reply_text(
        f"Thank you for playing {user.first_name}!! Searching for your price..."
    )
    update.message.reply_text(generate_btc_wallet())


def weather(update: Update, _: CallbackContext) -> None:
    """Generate Bitcoin address getting his balance"""
    user = update.effective_user
    assert update.message
    assert user

    update.message.reply_text(get_barcelona_data())


def help_command(update: Update, _: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    assert update.message
    update.message.reply_text(
        "You can controll me by sending these commands:\n\n"
        "/prices - get a list of top prices you can earn"
        ""
        "/play - get your lottery ticket"
    )
