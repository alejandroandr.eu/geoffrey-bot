ARG PYTHON_VERSION=3

FROM python:${PYTHON_VERSION}-alpine3.13

ARG POETRY_VERSION=1.1.6

ARG BOT_TOKEN
ENV BOT_BASEDIR "/usr/src/app"
ENV BOT_USER "geoffrey-bot"
ENV BOT_TOKEN ${BOT_TOKEN}

RUN apk add --no-cache \
        curl \
        gcc \
        libressl-dev \
        musl-dev \
        libffi-dev && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --profile=minimal && \
    source $HOME/.cargo/env && \
    pip install --no-cache-dir poetry==${POETRY_VERSION}
#     apk del \
#         curl \
#         gcc \
#         libressl-dev \
#         musl-dev \
#         libffi-dev

WORKDIR /app
COPY . .
RUN poetry install
# RUN set -ex && \
#     addgroup -S -g 800 ${BOT_USER} && \
#     adduser -S -D -G ${BOT_USER} -u 800 ${BOT_USER}
#
# USER ${BOT_USER}

CMD ["poetry", "run", "python", "main.py"]
