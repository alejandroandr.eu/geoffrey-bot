# Development environment

## Info about bots and API key
Information about bots: https://core.telegram.org/bots

To setup a development environment you need to get an API key. To get
an API key, talk to @botfather in telegram. He knows everything and
will give you a key

## Pyenv and Poetry
To setup the development environment, you need to install pyenv
(https://github.com/pyenv/pyenv) and poetry (https://python-poetry.org/docs/).
With Pyenv you can set a global or local python version. Poetry is 
the dependency management.

## Run with environment
Once you installed pyenv and poetry, set pyenv with `pyenv global 3.8.10` or
locally with `pyenv local 3.8.10`. Then install the environment with `poetry install`
and run it with `poetry run python main.py`


# geoffrey-bot

* [`python-telegram-bot` documentation](https://python-telegram-bot.readthedocs.io/en/stable/)

